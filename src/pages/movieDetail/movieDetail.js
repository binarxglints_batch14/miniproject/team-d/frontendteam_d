import { Route,useParams, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import React from "react";
import styles from './movieDetail.module.css'
import Character from './characters'
import Overview from './overview'
import Review from '../../components/review/review'
import NavbarUser from "../../components/navbarUser/navbarUser";
// import { useLocation } from "react-router";




export function Moviedetail(){
  
  // const location = useLocation();
  // const {pathname} = location;
  // const splitlocation = pathname.split("/");
  const runCallback = (cb) => {
    return cb();
  };

  let { id } = useParams()
  let [film, setFilm] = useState([]);
  let [char, setChar] = useState([]);
  let [details, setDetails] = useState([]);
  // let buttonMovieDetail =["overview", "character", "review"];

      useEffect(() => {
      axios
      .get(`https://miniproject-moviereviewapp.herokuapp.com/movies/${id}`)
      .then((response) => {
        console.log(response.data.data,"inininii");
        setChar(response.data.data.character)
        console.log(response.data.data.character, 'mamamamamama')
        setFilm(response.data.data.movie_info);
        setDetails(response.data.data)
      });
  }, []);
  console.log(film, "FILM DETAIL")

  const [buttonActive, setButtonActive] = useState();
  const buttonSelected = (e) => {
     setButtonActive(e.target.innerHTML)
    console.log(buttonActive, "uni button")
  }

  return(

      <React.Fragment>
        <NavbarUser/>
      <div className={styles.bannerContainer}>
          <img className={styles.imgBanner} src={details.poster} alt='imgPoster'/>
          <div className={styles.bannerInfo}>
                <h1>{details.title}</h1>
                {
                      runCallback(() => {
                        const row = [];
                        for (var i = 0; i < 5; i++) {
                          row.push( <svg width="49" height="38" viewBox="0 0 49 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M24.5 0L30.2251 14.5106H48.7519L33.7634 23.4787L39.4885 37.9894L24.5 29.0213L9.51148 37.9894L15.2366 23.4787L0.248058 14.5106H18.7749L24.5 0Z" fill={i < 3 ? "#EBCD00" : "#C4C4C4"}/>
                          </svg>);
                        }
                        return row;
                      })
                  }
      
                  <p className={styles.bannerText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                  sed do eiusmod   tempor incididunt ut labore et dolore magna aliqua.
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqu</p>
                        
                  <div className={styles.buttonBanner}>
                       <a className={styles.lWAT} href = {details.trailer}  target="_blank" rel="noreferrer" > <button className={styles.trailer}>Watch Trailer</button> </a>
                        <a className={styles.aWL}> <button className={styles.watchlist} >Add to Watchlist </button></a>
                  </div>
            </div>  
      </div>


      {/* ini diluar div banner */}
      <div className= {styles.container123}>
        {/* {buttonMovieDetail.map((buttonDetail) => (
        ))} */}

           <Link ClassName={styles.rv} to={`/moviedetail/${id}/`}><button className={buttonActive === "Overview" ? styles.ocrActive : styles.ocr } onClick={buttonSelected}>Overview</button></Link>
           <Link ClassName= {styles.rv}to={`/moviedetail/${id}/character`}><button className={buttonActive === "Character" ?  styles.ocrActive : styles.ocr} onClick={buttonSelected}>Character</button></Link>
           <Link ClassName= {styles.rv} to={`/moviedetail/${id}/review`}><button className={buttonActive === "Review" ?  styles.ocrActive : styles.ocr } onClick={buttonSelected}>Review</button></Link>
      </div>
      <div>
          <Route path={`/moviedetail/${id}/`} exact render={() => <Overview filmDetails={film} det = {details}/>} />
          <Route path={`/moviedetail/${id}/character`} render={() => <Character chart={char}/>} />
          <Route path={`/moviedetail/${id}/review`} render={() => <Review id={id}/>} />
      </div> 
        {/* ini diluar div banner */}
        
      </React.Fragment>
  )
}

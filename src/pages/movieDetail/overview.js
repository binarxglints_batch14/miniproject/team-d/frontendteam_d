import React from "react"
import styles from './movieDetail.module.css'


function Overview(props){

    
    return(
      <React.Fragment>
        <div className={styles.containerov}>
          <div className={styles.aaa}>
           <h1 className={styles.boom1}>Synopsis</h1>
           <p className={styles.smf}>{props.det.sypnopsis}</p>
          </div>

          <h1 className={styles.boom}>Movie Info</h1>
        <div className={styles.movieInfoContainer}>
            <p className={styles.smf}><b>Genre</b> : {props.filmDetails.Genre} </p>
            <p className={styles.smf}><b>Original Language</b> :{props.filmDetails['Original Language']}</p>
            <p className={styles.smf}><b>Director</b> : {props.filmDetails.Director}</p>
            <p className={styles.smf}><b>Producer</b> : {props.filmDetails.Producer}</p>
            <p className={styles.smf}><b>Writer</b> : {props.filmDetails.Writer}</p>
            <p className={styles.smf}><b>Distributor</b> : {props.filmDetails.Distributor}</p>
            <p className={styles.smf}><b>Production</b> : {props.filmDetails['Production Co']}</p>
            <p className={styles.smf}><b>Sound Mix</b> : {props.filmDetails['Sound Mix']}</p>
         </div>
    </div>
      </React.Fragment>
    )
  }
  
  export default Overview

import React from "react"
import styles from './movieDetail.module.css'

function Character(props){

    console.log(props, 'jajajajajaj')
  return(
    <React.Fragment>
      <h1>Character</h1>
      <div className={styles.charContainer}>
            {props.chart.map((char) => (
              <div key={char._id} className={styles.divCharBox}>
                  <img 
                  className={styles.imgProfilePath}
                  src={char.picture}
                  alt='char'
                  />
                <div className={styles.charInfo}>
                    <p className={styles.nameChar}>{char.name}
                    </p>
                    </div>
                </div>
            ))}
      </div>
      </React.Fragment>
    )
  }
  
  export default Character;

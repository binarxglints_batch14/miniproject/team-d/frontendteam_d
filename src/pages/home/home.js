import FilterCategory from "../../components/filterCategory/filterCategory";
import React, { useState, useEffect } from "react";
import axios from "axios";
import styles from "./home.module.css";
import Moviecarousel from "../../components/carousel/carousel";
import Register from '../../components/register/register';
import spinner from './img/spinner.gif';
import prev from './img/prevarrow.png';
import next from './img/nextarrow.png';
import { Link } from "react-router-dom";
import NavbarUser from "../../components/navbarUser/navbarUser";


 function Home() {
   
      let [film, setFilm] = useState([]);
      let [pages, setPages] = useState(1);
      let [genre, setGenre] = useState('');
      let [total,setTotal] = useState();
      let [nextArrow,setNextArrow] = useState(true);
      let [prevArrow,setPrevArrow] = useState(false);
      let [loading,setLoading] = useState(true)
      let param = '';

      if(genre === '' || genre == 'all'){
        param = '';
      }else{
        param = `genre=${genre}`;
      }
        

      useEffect(() => {
        axios
          .get(`https://miniproject-moviereviewapp.herokuapp.com/movies/show/genre?page=${pages}&limit=10&${param}`)
          .then((response) => {
            
              setFilm(response.data.data.docs);
              setTotal(response.data.data.totalPages);
              setNextArrow(response.data.data.hasNextPage);
              setPrevArrow(response.data.data.hasPrevPage);
              setLoading(false)
          
          });
      }, [pages,param]);
 

      function pageOnclick(evt){
        setPages(evt);
        // console.log('dari onclick' + pages);
      }

      function changeGenre(param){
        setGenre(param)
        setPages(1);
      }

      const runCallback = (cb) => {
        return cb();
      };

      function pageNext() {
        setPages(pages + 1);
      }

      function pagePrev() {
        setPages(pages - 1);
      }
  // console.log(userName, "uuuuserr")
      return (
        <React.Fragment>
      
          <NavbarUser/>
          <Moviecarousel/>
          <div className={styles.browseContainer}>
        
              <h1 className={styles.browse}>Browse by Category</h1>
              <FilterCategory changeGenre={setGenre} genre={genre}/>
              
          </div>
      
          <div className={styles.divContainer}>

              {loading ? <div><img src={spinner}/></div>: ''}
              {film.map((film) => (
                  <div key={film._id} className={styles.divBox}>
                        <Link to={`/moviedetail/${film._id}`}  style={{ textDecoration: 'none' }}>
                            <div className={styles.imgContainer}>
                                  <img
                                    className={styles.imgPoster}
                                    src={film.poster}
                                    alt="img"
                                  />
                            </div>

                            <div className={styles.movieInfo}>
                                <p className={styles.title}>{film.title} <br/></p>
                                {film.movie_info.Genre.map((genre) => (<div className={styles.genre}  >{genre},</div>))}
                            </div> 
                        </Link>
                  </div>
              ))}
          </div>

          <div className={styles.pageinationContainer}>
              <button className={styles.prevNext} disabled={prevArrow === false} onClick={() => pagePrev()}><img src={prev}/></button>
              {
                    runCallback(() => {
                        const row = [];
                        for (let x = 1; x <= total; x++) {
                              if(pages === x) {
                                row.push( <button className={styles.pagesButton}   key={x} onClick={() => pageOnclick(x)} >{x}</button> );
                              }else {
                                row.push( <button className={styles.pagesButtonActive}   key={x} onClick={() => pageOnclick(x)} >{x}</button> );
                              }
                        }
                        return row;
                    })
                }

              <button className={styles.prevNext} disabled={nextArrow === false} onClick={() => pageNext()}><img src={next}/></button>
          </div>
              
        </React.Fragment>
      );
}

export default Home;
import {
  BrowserRouter,
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import Home from '../pages/home/home';
import { Moviedetail } from "../pages/movieDetail/movieDetail";


function Routes() {
  return(
  <BrowserRouter>
    <Router>
      <Switch>
        <Route exact path="/" component= {Home}/>
        <Route path="/movieDetail/:id" component = {Moviedetail}/>
      </Switch>
    </Router>
    </BrowserRouter>
  )
}

export default Routes;

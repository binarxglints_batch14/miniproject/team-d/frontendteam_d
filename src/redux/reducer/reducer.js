import {combineReducers} from 'redux';
import ReviewReducer from '../../components/review/reviewReducer'

const rootReducer = combineReducers({
   ReviewReducer
});

export default rootReducer;
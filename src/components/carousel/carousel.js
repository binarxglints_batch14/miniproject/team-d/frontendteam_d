
import gambar3 from './images/gambar3.jpg';
import gambar1 from './images/gambar1.jpg';
import gambar2 from './images/gambar2.jpg';

import './styles/carousel.css';
import { Carousel } from 'react-bootstrap';

function Moviecarousel() {

  let images = [gambar1,gambar2,gambar3];


  return (
    
      <Carousel style={{marginTop: "60px"}}
        interval={3500}
        >
          {images.map((img) => <Carousel.Item  >
          <img
            className="d-block w-100 heightCarousel"
            src={img}
            alt="First slide"
          
          />
        </Carousel.Item>)}
        
      </Carousel>
     
  )
}

export default Moviecarousel;
import React from "react";
import logo from './logo.png'
import styles from './login.module.css'
import { useState } from 'react';
import axios from 'axios';

export default function Login(props) {

  const[values,setValues] = useState({
    email: "",
    password: ""
  })
  // const [token, setToken] = useState("")

  const handlechange = (e) => {
    const value = e.target.value
    const name = e.target.name
    setValues({ ...values, [name]: value });
  }


  const handleSubmit = (e) =>{
    e.preventDefault()
    axios
    .post("https://miniproject-moviereviewapp.herokuapp.com/login",
    {
      "email" : values.email,
      "password": values.password

    },)
  .then((Response)=> {
    const token = Response.data.token
    console.log(Response,"nanananana")
    localStorage.setItem('tokenNichneng', token);
    props.closeModal();
  })
  .catch((Error) => {
    console.log(Error, "wah ini eror")
  })
  }

  const handleGetUser = () =>{
    const token = localStorage.setItem()
  }


return(
    <div className={styles.divContainer}>
      <div className={styles.boxLogo}>
          <img className={styles.logo} src={logo} alt='Logo'/> 
          <h3>MilanTV</h3>
      </div>
   
      <form className={styles.formRegister} onSubmit = {handleSubmit}>
    
        <label>Email</label> 
        <input type="email" onChange={handlechange} name='email' type="email" value={values.email} required/>
        <label>Password</label>
        <input type="password"  onChange={handlechange} name='password' type="password" value= {values.password} required/>
        
      <button className={styles.buttonLogin}  type="Submit" >Sign In</button>
      
      
      </form>
    <p >Don't have an account? 
       <span onClick={props.openRegister}  style={{color: "#FE024E", cursor: "pointer"}}> Sign Up</span>  
      </p>
 </div>

  )
}

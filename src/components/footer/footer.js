import React from "react";
import styles from './footer.module.css'
import logo from './footer-brand.png'
import googlePlay from './googlePlay.png'
import appStore from './appleStore.png'
import facebook from './fb.png'
import pinterest from './pinters.png'
import instagram from './ig.png'

export default function Footer() {

    return (
    <div className={styles.footer}>
    <div className={styles.divContainer}>
        <div className={styles.footerLogo}>
           
                    <img className={styles.logoMilan} src={logo} alt='logo'/>
        
            
            
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                Lorem Ipsum has been the industrys standard. printing and typesetting industry. 
                Lorem Ipsum has been the industry s standard
                </p>
        </div>
        <div className={styles.infoFeature}>
            <ul className={styles.box}>
                <li> <a href='#'>Tentang Kami</a></li>
                <li> <a href='#'>Blog</a></li>
                <li> <a href='#'>Layanan</a></li>
                <li> <a href='#'>Karir</a></li>
                <li> <a href='#'>Pusat Media</a></li>
            </ul>  
        </div>
        <div className={styles.boxBacklinks}>
            <div className ={styles.boxDownload}>
                <h4>Download</h4>
                <br/>
                    <a className={styles.gPlay} href='#'><img src={googlePlay} alt='googlePlay'/></a>
                    <a className={styles.aStore} href='#'><img src={appStore} alt='appStore'/></a>
            </div>
            <div className={styles.boxSocialMedia}>
                <h4>Social Media</h4>
                    <a className={styles.fb} href='#'><img src={facebook} alt='facebook'/></a>
                    <a className={styles.pt} href='#'><img src={pinterest} alt='pinterest'/></a>
                    <a className={styles.ig} href='#'><img src={instagram} alt='instagram'/></a>
                
             </div>
        </div>
    </div>
    <div className={styles.copyrightText}>
        <p>Copyright © 2000-202 MilanTV. All Rights Reserved</p>
    </div>
</div>

)}
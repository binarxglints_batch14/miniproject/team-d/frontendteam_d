import styles from './styles/review.module.css';
import kucing from './kucing.jpg';
import lupi from './lupi.jpg';
import React from 'react';
import { useEffect } from 'react';
import {useState} from 'react'
import axios from 'axios';



function Review(props) {

    let Token = localStorage.getItem("tokenNichneng")
    const [res, setRes]= useState([])
    const [resReview, setResReview] = useState([])
    const [page, setPage] = useState(1)
    const [userName, setUserName] = useState("")


    const id = props.id
    console.log(id, "ini id review")


    const[values,setValues] = useState({
      userRating: 0,
      userReview: ""
    })

    const handlechange = (e) => {
      const value = e.target.value
      const name = e.target.name
      setValues({ ...values, [name]: value });
    }

    //  useEffect untuk Username 

    useEffect(() => {
      if (Token !== null){
      axios.get("https://miniproject-moviereviewapp.herokuapp.com/me",{
          headers : {
            "AUTHORIZATION" : `Bearer ${Token}`
          }
        })
        .then((Res)=>{
          setUserName(Res.data.data.name)
        })
      }else{
        setUserName("")
      }
      }, [userName]);

      const removeToken = () =>{
        localStorage.removeItem("tokenNichneng")
        window.location.reload()
      }

    //  ini useEffect untuk komentar 

    useEffect(() => {
      axios
        .get(`https://miniproject-moviereviewapp.herokuapp.com/review/${id}?page=${page}&limit=2`)
        .then((response) => {
          setRes(response.data.data)
        
        });
    }, [resReview, page]);


      const runCallback = (cb) => {
        return cb();
      };


    function handleSubmit (e){
        e.preventDefault();
        document.querySelector('form').reset()
        axios
        .post(`https://miniproject-moviereviewapp.herokuapp.com/review/${id}`,
        {
          "userRating" : values.userRating,
          "userReview": values.userReview,
        },{
          headers : {
            "AUTHORIZATION" : `Bearer ${Token}`
          }
        })
        .then((Response)=> {
          setResReview(Response)
          console.log(Response,"mas wahyu raja redux")
        })
        .catch((Error) => {
          console.log(Error, "wah ini eror")
        })
          
    };

    const loadMore = () =>{
      setPage(page + 1)
    }


      return(
        <React.Fragment>
        <div className={styles.reviewContainer}>
            <form className={Token !== null ? styles.reviewUpdate : styles.reviewUpdatenone} onSubmit={handleSubmit}>
              <div className={styles.avaContainer}>
                <img className={styles.avaImg} src={kucing} alt="avatar"/>
              </div>
              <div className={styles.reviewInput}>
                <p className={styles.userName}>{userName}</p>
              <div>
                {
                    runCallback(() => {
                      const row = [];
                      for (var i = 1; i <= 5; i++) {
                        row.push( 
                          <div className={styles.checkRating}>
                          <input onChange={handlechange} name = "userRating" className={styles.checkboxRating} type="radio" value={i}  required /> 
                            <svg className={styles.star} width="22" height="22" viewBox="0 0 49 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M24.5 0L30.2251 14.5106H48.7519L33.7634 23.4787L39.4885 37.9894L24.5 29.0213L9.51148 37.9894L15.2366 23.4787L0.248058 14.5106H18.7749L24.5 0Z" fill={values.userRating >= i? "yellow" :"#CDCDCD" }/>
                            </svg>
                          </div>
                        );
                      }
                      return row;
                    })
                }
                    </div>
                      <input className={styles.textareaInput} name = "userReview" type = "text" placeholder="Leave a review" onChange={handlechange}/>
                    </div>
                    <div className={styles.addreviewContainer}>
                    <button className={styles.buttonReview} type="submit"><p>+</p></button>
                    </div>
            </form>
        </div>

                      {res.map((review) => 
                              
                              <div key = {review}className={styles.reviewContainer}>
                              <div className={styles.reviewUpdate}>
                                <div className={styles.avaContainer}>
                                  <img className={styles.avaImg} src={lupi} alt="avatar"/>
                                </div>
                                
                                <div className={styles.reviewInput}>
                                  <p className={styles.userName}>{review.username}</p>
                                <p className={styles.reviewValue}>{review.userReview}</p>
                              
                                </div>
                              </div>
                          </div>
                        
                    )}
                    <div className={styles.loadContainer}>
                <button className={styles.loadMore} onClick={loadMore} >Load More</button>
                </div>
        </React.Fragment>
      )
}
// const mapStateToProps = (store)  => ({
//   add : store.ReviewReducer.review,
//   data : store.ReviewReducer.data
// });

// const mapDispatchToProps = (dispatch) => {
// return bindActionCreators({
// changeReview : (text) => dispatch(changeReview(text)),
// addReview: (review)=> dispatch(addReview(review)),
// getReview: getReview
// }, dispatch);
// }

export default Review
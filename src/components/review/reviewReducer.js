import * as types from '../review/reviewActionTypes'

const initialStore = {
    review : ['wah asik nih filmnya','bagus mantep','asyik boyy'],
    data : [],
    text: [],
    loading: false,
    error: ""
}

function ReviewReducer (store = initialStore, action){
    switch(action.type){
        case types.CHANGE_INPUT_REVIEW:
            return {...store, text : action.payload.text}
        case types.ADD_REVIEW_TEXT:
             return { ...store, review:store.review.concat(action.payload.review)};
        case types.GET_REVIEW_USER:
            return {...store, loading: true}
        case types.GET_REVIEW_SUCCESS:
            return {...store, loading: false, data:store.data.concat(action.payload.data)}
        case types.GET_REVIEW_FAILURE:
            return {...store, loading: false, error: action.error}
        default:
            return store
    }
}
export default ReviewReducer
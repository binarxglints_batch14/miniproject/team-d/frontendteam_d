import styles from './register.module.css';
import logo from './logo.png';
import { useState } from 'react';
import axios from 'axios';

function Register(props) {

  const[values,setValues] = useState({
    name: "",
    email: "",
    password: ""
  })

  const handlechange = (e) => {
    const value = e.target.value
    const name = e.target.name
    setValues({ ...values, [name]: value });
   
  }
 

  const handleSubmit = (e) =>{
    e.preventDefault()
    axios
    .post("https://miniproject-moviereviewapp.herokuapp.com/register",
    {
      "name" : values.name,
      "email" : values.email,
      "password": values.password

    })
  .then((Response)=> {
    console.log(Response,"nanananana");
    props.closeModal();
  })
  .catch((Error) =>{
    console.log(Error, "wah ini eror");
  })
  }


 
 console.log(values, 'values')
  return(
    <div className={styles.divContainer}>
      <div className={styles.boxLogo}>
          <img className={styles.logo} src={logo}/> 
          <h3>MilanTV</h3>
      </div>
   
      <form className={styles.formRegister} onSubmit= {handleSubmit}>
    
        <label>Full Name</label>
        <input className={styles.inputRegister} onChange={handlechange} name='name' type="text" value={values.name}  required/>
        <label>Email</label> 
        <input className={styles.inputRegister}  onChange={handlechange} name='email' type="email" value={values.email}   required/>
        <label>Password</label>
        <input className={styles.inputRegister}  onChange={handlechange} name='password' type="password" value={values.password}   required/>
        
      <button className={styles.buttonRegister}   type="Submit">Sign Up</button>
      
      
      </form>
    <p className={styles.registerConfirm} >Already have an account? 
       <span onClick={props.openRegister} style={{color: "#FE024E", cursor: "pointer"}}> Sign In</span>  
      </p>
 </div>

  )
}

export default Register;
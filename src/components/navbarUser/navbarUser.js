import styles from './navbarUser.module.css';
import logo from './logo1.png'
import Modal from 'react-modal';
import Register from '../register/register'
import * as React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import MovieTitle from './data'
import { useEffect, useState } from "react";
import Login from '../login/login';
import axios from "axios";
import { Link } from 'react-router-dom'

Modal.setAppElement('#root')

function NavbarUser(props) {
  
    const [modalIsOpen,setIsOpen] = useState();
    const [regForm, setRegForm] = useState(false);
    const [userName, setUserName] = useState("")
    const [drop, setDrop] = useState(false);
    
    let Token = localStorage.getItem("tokenNichneng")
    console.log(Token, "ini adlah token dari home")
  
    function openModal() {
      setIsOpen(true);
    }


    function closeModal(){
      setIsOpen(false);
    }

    function afterOpenModal(e) {
      // references are now sync'd and can be accessed.
    }

    function onAfterRegister(){
      
    }

    const openRegister = () => {
      if(regForm === false) {
        setRegForm(true)
      }else{
        setRegForm(false)
      }
    }

    useEffect(() => {
        axios.get("https://miniproject-moviereviewapp.herokuapp.com/me",{
            headers : {
              "AUTHORIZATION" : `Bearer ${Token}`
            }
          })
        .then((Res)=>{
          console.log(Res, "ini dari get username")
          setUserName(Res.data.data.name)
          
        })
        .catch((error) => console.log (error) )
    }, [Token]);

    const removeToken = () =>{
      localStorage.removeItem("tokenNichneng")
      window.location.reload()
    }

  //   useEffect(() => {
  //     window.location.reload()
  // }, [userName]);
    // if(userName !== "") {
    //   window.location.reload()
    // }


// console.log(props, "ini dari home")
   
  const dropDown = () =>{
    if(drop === false){
      setDrop(true);
    }else{
      setDrop(false)
    }
  }


    return (
        <div className={styles.navbarContainer}  >
            <nav>
                <Link to={`/`}><img className={styles.logo} src={logo}  alt=''/></Link>
                {/* <input className = {styles.input}type = "text" placeholder = "Seacrh Movie" /> */}
                <Autocomplete style={{marginTop: "5px"}}
                  disablePortal
                  id="combo-box-demo"
                  options={MovieTitle}
                  sx={{ width: 700 }}
                  renderInput={(params) => <TextField {...params} label="Search Movie" size="small" />}
                />
                
                {Token === null? <div className = {styles.txtSignin} onClick={openModal}>sign in</div> : <div onClick={dropDown} className= {styles.avatarImg}>
                  {userName.charAt(0)}
                  <div className={drop === false ? styles.dropdownNone : styles.dropdownctn } >
                        <p style={{ color: 'black' }}>{userName}</p>
                        <p>Profil</p>
                        <p>Setting</p>
                        <p>Help</p>
                        <p onClick = {removeToken}>Sing Out</p>
                    </div>
                </div>
                }

                <Modal className={styles.Modal}
                  isOpen={modalIsOpen}
                  onAfterOpen={afterOpenModal} 
                  onRequestClose={closeModal}>
                  <div className={styles.Register}>
                    {regForm === false? <Login openRegister={openRegister} closeModal={closeModal}/> : <Register openRegister={openRegister} closeModal={closeModal}/> }
                  
                  
                  {/* <button className={styles.btnClose} onClick={closeModal}>X</button> */}
                  </div>
                 </Modal>
              
            </nav>
        </div>
  );
}
 
 export default NavbarUser;
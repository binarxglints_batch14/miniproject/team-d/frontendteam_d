import styles from './filterCategory.module.css';
import React, { useCallback, useState, useEffect } from 'react';

import axios from "axios";
function FilterCategory({genre, changeGenre}){

  const [isActive, setActive] = useState(false);
  let[genres,setGenres] = useState([]);

  
  const selectGenre = useCallback(event => {
    
    changeGenre(event)
    if(event == 'all') {
      setActive(true)
    }else {
      setActive(false)
    }
   
    console.log('aktip',isActive);
  }, [changeGenre])

useEffect(() => {
  axios
    .get(`https://miniproject-moviereviewapp.herokuapp.com/movies/find/genre`)
    .then((response) => {
      console.log('dari useefect',response.data.data);
      setGenres(response.data.data)  
    });
}, []);

console.log('dari kategori',genre);


  return (
    <div className={styles.categoryContainer}>
              <button onClick={(event) => selectGenre('all')} className={isActive === true || genre === '' ?  styles.buttonActive : styles.buttonCategory}>all</button>
           {genres.map((genres) => (
            
             <button onClick={() => selectGenre(genres)}  className={genres === genre ?  styles.buttonActive : styles.buttonCategory} >{genres}</button>
          
        ))}
    </div>
  )
}
export default FilterCategory;
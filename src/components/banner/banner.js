import styles from './styles/banner.module.css';
import ImgBanner from './minion.jpg';
import { Link } from 'react-router-dom';
import React from 'react';
import Review from '../review/review'
function Banner() {
  const runCallback = (cb) => {
    return cb();
  };


  
  return(

    <React.Fragment>
    <div className={styles.bannerContainer}>
        <img className={styles.imgBanner} src={ImgBanner} alt='image banner' />
        <div className={styles.bannerInfo}>
          <h1>Despicable Me 2</h1>
          {
                runCallback(() => {
                  const row = [];
                  for (var i = 0; i < 5; i++) {
                    row.push( <svg width="49" height="38" viewBox="0 0 49 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24.5 0L30.2251 14.5106H48.7519L33.7634 23.4787L39.4885 37.9894L24.5 29.0213L9.51148 37.9894L15.2366 23.4787L0.248058 14.5106H18.7749L24.5 0Z" fill={i < 3 ? "#EBCD00" : "#C4C4C4"}/>
                    </svg>);
                  }
                  return row;
                })
            }
        
          <p className={styles.bannerText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod   tempor incididunt ut labore et dolore magna aliqua.
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqu</p>
            <div className={styles.buttonBanner}>
                  <Link target="_blank" to={{ pathname: "https://www.youtube.com/watch?v=yM9sKpQOuEw&ab_channel=UniversalPictures" }} style={{ textDecoration: 'none' }}>
                      <button className={styles.trailer} >Watch Trailer</button>  
                      </Link>
                      <button className={styles.watchlist}>Add to Watchlist</button>  
            </div>
        </div>
    </div>
    <Review/>
    </React.Fragment>
  )
}

export default Banner;
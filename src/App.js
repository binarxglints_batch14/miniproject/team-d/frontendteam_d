
import './App.css';
import Routes from './routes/route';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavbarUser from './components/navbarUser/navbarUser'
import Footer from './components/footer/footer'


function App() {
  return (
    <div className="App">
      {/* <NavbarUser/> */}
      <Routes/>
      <Footer/>
    </div>
  );
}

export default App;
